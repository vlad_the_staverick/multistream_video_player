var main_player_0 = videojs('main_window_0',{controls : false,});
var main_player_1 = videojs('main_window_1',{controls : false,});
var mini_player_0 = videojs('mini_window_0',{controls : false,});
var mini_player_1 = videojs('mini_window_1',{controls : false,});

function play(){
    main_player_0.play();
    main_player_1.play();
    mini_player_0.play();
    mini_player_1.play();
}

function pause(){
    main_player_0.pause();
    main_player_1.pause();
    mini_player_0.pause();
    mini_player_1.pause();
}

function swap(id){
    document.getElementById("main_window_0").style.display="none";
    document.getElementById("main_window_1").style.display="none";

    document.getElementById(id).style.display="block";
}

swap('main_window_0');
play();